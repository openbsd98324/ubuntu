# ubuntu

## Current 

https://releases.ubuntu.com/22.04.3/ubuntu-22.04.3-desktop-amd64.iso

## Get Ubuntu Current Downloads

https://releases.ubuntu.com/22.04.3/ubuntu-22.04.3-desktop-amd64.iso



## Releases

````

List of releases

   Ubuntu Website release cycle page

  Current

   Version            Code name       Docs          Release           End of Standard Support End of Life 
   Ubuntu 22.04 LTS   Jammy Jellyfish Release Notes April 21, 2022    April 2027              April 2032  
   Ubuntu 20.04.4 LTS Focal Fossa     Changes       February 24, 2022 April 2025              April 2030  
   Ubuntu 20.04.3 LTS Focal Fossa     Changes       August 26, 2021   April 2025              April 2030  
   Ubuntu 20.04.2 LTS Focal Fossa     Changes       February 4, 2021  April 2025              April 2030  
   Ubuntu 20.04.1 LTS Focal Fossa     Changes       August 6, 2020    April 2025              April 2030  
   Ubuntu 20.04 LTS   Focal Fossa     Release Notes April 23, 2020    April 2025              April 2030  
   Ubuntu 18.04.6 LTS Bionic Beaver   Changes       September 17.2021 April 2023              April 2028  
   Ubuntu 18.04.5 LTS Bionic Beaver   Changes       August 13, 2020   April 2023              April 2028  
   Ubuntu 18.04.4 LTS Bionic Beaver   Changes       February 12, 2020 April 2023              April 2028  
   Ubuntu 18.04.3 LTS Bionic Beaver   Changes       August 8, 2019    April 2023              April 2028  
   Ubuntu 18.04.2 LTS Bionic Beaver   Changes       February 15, 2019 April 2023              April 2028  
   Ubuntu 18.04.1 LTS Bionic Beaver   Changes       July 26, 2018     April 2023              April 2028  
   Ubuntu 18.04 LTS   Bionic Beaver   Release Notes April 26, 2018    April 2023              April 2028  
   Ubuntu 16.04.7 LTS Xenial Xerus    Changes       August 13, 2020   April 2021              April 2026  
   Ubuntu 16.04.6 LTS Xenial Xerus    Changes       February 28, 2019 April 2021              April 2026  
   Ubuntu 16.04.5 LTS Xenial Xerus    Changes       August 2, 2018    April 2021              April 2026  
   Ubuntu 16.04.4 LTS Xenial Xerus    Changes       March 1, 2018     April 2021              April 2026  
   Ubuntu 16.04.3 LTS Xenial Xerus    Changes       August 3, 2017    April 2021              April 2026  
   Ubuntu 16.04.2 LTS Xenial Xerus    Changes       February 16, 2017 April 2021              April 2026  
   Ubuntu 16.04.1 LTS Xenial Xerus    Changes       July 21, 2016     April 2021              April 2026  
   Ubuntu 16.04 LTS   Xenial Xerus    Release Notes April 21, 2016    April 2021              April 2026  
   Ubuntu 14.04.6 LTS Trusty Tahr     Changes       March 7, 2019     April 2019              April 2024  
   Ubuntu 14.04.5 LTS Trusty Tahr     Changes       August 4, 2016    April 2019              April 2024  
   Ubuntu 14.04.4 LTS Trusty Tahr     Changes       February 18, 2016 HWE August 2016         April 2024  
   Ubuntu 14.04.3 LTS Trusty Tahr     Changes       August 6, 2015    HWE August 2016         April 2024  
   Ubuntu 14.04.2 LTS Trusty Tahr     Changes       February 20, 2015 HWE August 2016         April 2024  
   Ubuntu 14.04.1 LTS Trusty Tahr     Changes       July 24, 2014     April 2019              April 2024  
   Ubuntu 14.04 LTS   Trusty Tahr     Release Notes April 17, 2014    April 2019              April 2024  

     * Release announcements are posted on the ubuntu-announce mailing list.

  Future

   Version            Code name       Docs          Release           End of Standard Support 
   Ubuntu 22.04.1 LTS Jammy Jellyfish Changes       August 11, 2022   April 2027              
   Ubuntu 20.04.5 LTS Focal Fossa     Changes       September 1, 2022 April 2025              
   Ubuntu 22.10       Kinetic Kudu    Release Notes October 20, 2022  July 20, 2023           

  Extended Security Maintenance

   Extended Security Maintenance (ESM) provides security updates on Ubuntu LTS releases for additional 5 years. It is available with the Ubuntu Advantage subscription or a Free subscription.

   Version          Detailed ESM Coverage  Start of ESM   # of Years End of Life 
   Ubuntu 20.04 ESM To be announced        April 2025     5 years    April 2030  
   Ubuntu 18.04 ESM To be announced        April 2023     5 years    April 2028  
   Ubuntu 16.04 ESM SecurityTeam/ESM/16.04 April 2021     5 years    April 2026  
   Ubuntu 14.04 ESM SecurityTeam/ESM/14.04 April 25, 2019 5 years    April 2024  
   Ubuntu 12.04 ESM SecurityTeam/ESM/12.04 April 28, 2017 2 years    April 2019  

  End of Life

   Version            Code name              Docs            Release           End of Life             
   Ubuntu 21.10       Impish Indri           Release Notes   October 14, 2021  July 14, 2022           
   Ubuntu 21.04       Hirsute Hippo          Release Notes   April 22, 2021    January 20, 2022        
   Ubuntu 20.10       Groovy Gorilla         Release Notes   October 22, 2020  July 22, 2021           
   Ubuntu 19.10       Eoan Ermine            Release Notes   October 17, 2019  July 17, 2020           
   Ubuntu 19.04       Disco Dingo            Release Notes   April 18, 2019    January 23, 2020        
   Ubuntu 18.10       Cosmic Cuttlefish      Release Notes   October 18, 2018  July 18, 2019           
   Ubuntu 17.10       Artful Aardvark        Release Notes   October 19, 2017  July 19 2018            
   Ubuntu 17.04       Zesty Zapus            Rel             April 13, 2017    January 13, 2018        
   Ubuntu 16.10       Yakkety Yak            Rel             October 13, 2016  July 20, 2017           
   Ubuntu 15.10       Wily Werewolf          Rel             October 22, 2015  July 28, 2016           
   Ubuntu 15.04       Vivid Vervet           Rel             April 23, 2015    February 4, 2016        
   Ubuntu 14.10       Utopic Unicorn         Rel             October 23, 2014  July 23, 2015           
   Ubuntu 13.10       Saucy Salamander       Rel             October 17, 2013  July 17, 2014           
   Ubuntu 13.04       Raring Ringtail        Rel             April 25, 2013    January 27, 2014        
   Ubuntu 12.10       Quantal Quetzal        Tech / Rel      October 18, 2012  May 16, 2014            
   Ubuntu 12.04.5 LTS Precise Pangolin       Rel             August 7, 2014    April 28, 2017          
   Ubuntu 12.04.4 LTS Precise Pangolin       Changes         February 6, 2014  HWE August 8, 2014      
   Ubuntu 12.04.3 LTS Precise Pangolin       Changes         August 23, 2013   HWE August 8, 2014      
   Ubuntu 12.04.2 LTS Precise Pangolin       Changes         February 14, 2013 HWE August 8, 2014      
   Ubuntu 12.04.1 LTS Precise Pangolin       Changes         August 24, 2012   April 28, 2017          
   Ubuntu 12.04 LTS   Precise Pangolin       Tech / Rel      April 26, 2012    April 28, 2017          
   Ubuntu 11.10       Oneiric Ocelot         Tech / Rel      October 13, 2011  May 9, 2013             
   Ubuntu 11.04       Natty Narwhal          Tech / Rel      April 28, 2011    October 28, 2012        
   Ubuntu 10.10       Maverick Meerkat       Tech / Rel      October 10, 2010  April 10, 2012          
   Ubuntu 10.04.4 LTS Lucid Lynx             Changes         February 16, 2012 May 9, 2013 (Desktop)   
                                                                               April 30, 2015 (Server) 
   Ubuntu 10.04.3 LTS Lucid Lynx             Changes         July 21, 2011     
   Ubuntu 10.04.2 LTS Lucid Lynx             Changes         February 18, 2011 
   Ubuntu 10.04.1 LTS Lucid Lynx             Changes         August 17, 2010   
   Ubuntu 10.04 LTS   Lucid Lynx             Tech / Rel      April 29, 2010    
   Ubuntu 10.04       Lucid Lynx (Desktop)   Changes         February 16, 2012 May 9, 2013             
   Ubuntu 9.10        Karmic Koala           Tech / Rel      October 29, 2009  April 30, 2011          
   Ubuntu 9.04        Jaunty Jackalope       Tech / Rel      April 23, 2009    October 23, 2010        
   Ubuntu 8.10        Intrepid Ibex          Rel             October 30, 2008  April 30, 2010          
   Ubuntu 8.04.4 LTS  Hardy Heron (Server)   Changes         January 28, 2010  May 9, 2013             
   Ubuntu 8.04.3 LTS  Hardy Heron            Changes         July 16, 2009     
   Ubuntu 8.04.2 LTS  Hardy Heron            Changes         January 22, 2009  
   Ubuntu 8.04.1 LTS  Hardy Heron            Hardy Heron     July 3, 2008      
   Ubuntu 8.04 LTS    Hardy Heron            Hardy Heron/Rel April 24, 2008    
   Ubuntu 8.04        Hardy Heron (Desktop)  Rel             April 24, 2008    May 12, 2011            
   Ubuntu 7.10        Gutsy Gibbon           Rel             October 18, 2007  April 18th, 2009        
   Ubuntu 7.04        Feisty Fawn            Rel             April 19, 2007    October 19, 2008        
   Ubuntu 6.10        Edgy Eft                               October 26, 2006  April 26, 2008          
   Ubuntu 6.06.2 LTS  Dapper Drake (Server)                  January 21, 2008  June 1, 2011            
   Ubuntu 6.06.1 LTS  Dapper Drake                           August 10, 2006   
   Ubuntu 6.06 LTS    Dapper Drake           Rel             June 1, 2006      
   Ubuntu 6.06        Dapper Drake (Desktop) Rel             June 1, 2006      July 14, 2009           
   Ubuntu 5.10        Breezy Badger          Rel             October 12, 2005  April 13, 2007          
   Ubuntu 5.04        Hoary Hedgehog                         April 8, 2005     October 31, 2006        
   Ubuntu 4.10        Warty Warthog                          October 26, 2004  April 30, 2006          

     * The content of these old releases can be accessed at the old Ubuntu releases directory.

Management of releases

  Support length

     * Regular releases are supported for 9 months.
     * Packages in main and restricted are supported for 5 years in long term support (LTS) releases. Flavors generally support their packages for 3 years in LTS releases but there are exceptions. See the release notes for
       specific details.

  Release cadence

     * Releases are published in a time based fashion, every 6 months, following a planned schedule.

  Release team

     * Visit the release team page.

  To find your release

   Open a terminal and type

     lsb_release -a

````


## Desktop

20.10

![](media/Ubuntu_20.10_Groovy_Gorilla_Desktop.png)



22.04 

vm virtualbox vmdk

![](media/1691152338-screenshot.png)

22.10



![](https://gitlab.com/openbsd98324/dell-precision-3561/-/raw/main/medias/1671869703-1-22.10.jpg)


![](media/1691147760-screenshot.png)




## Downloads

 
Direct link:

````
ftp://ftp.ubuntu.com/releases/22.04.1/ubuntu-22.04.1-desktop-amd64.iso
````

````
https://old-releases.ubuntu.com/releases/groovy/ubuntu-20.10-desktop-amd64.iso
````




http://old-releases.ubuntu.com/releases/groovy/ubuntu-20.10-desktop-amd64.iso


https://mirror.kumi.systems/ubuntureleases/22.10/ubuntu-22.10-desktop-amd64.iso




